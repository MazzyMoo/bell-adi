import React from "react";

const TableRow = (props) => {
	// console.log("rows:");
	// console.log(props.row);
	return (
		<tr>
			<th>{props.row.id}</th>
			<th>{props.row.title}</th>
			<th>{props.row.body}</th>
		</tr>
	);
};

export default TableRow;
