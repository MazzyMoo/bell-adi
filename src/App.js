//(requirements can be found in readmed file)

import React from "react";
import axios from "axios";
import Table from "react-bootstrap/Table";
import TableRows from "./TableRows/TableRows";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

//Because we only need to do a simple task, I decided to not create a Table component and use App.js as a skeleton
export default class App extends React.Component {
	constructor() {
		super();
		this.state = { posts: [] };
	}

	//mount posts to our state
	componentDidMount() {
		axios.get("https://jsonplaceholder.typicode.com/posts").then((res) => {
			this.setState({ posts: res.data });
		});
	}
	//the UI can be much better, I decided to be minimal with it
	render() {
		return (
			<div className="container" style={{ paddingTop: 20 }}>
				<h3>Sample solution: </h3>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>#</th>
							<th>Title</th>
							<th>Body</th>
						</tr>
					</thead>
					<tbody>
						{this.state.posts.map((
							item,
							index //populate all data to table rows
						) => (
							<TableRows row={item} key={index} />
						))}
					</tbody>
				</Table>
			</div>
		);
	}
}
